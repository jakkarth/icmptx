# This is the ICMPTX program.

This software is most recently available from <https://codeberg.org/jakkarth/icmptx>

ICMPTX is a program that allows a user with root privledges to create a
virtual network link between two computers, encapsulating data inside of
ICMP packets.

### License
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
[GNU General Public License](./LICENSE) for more details.

You should have received a copy of the GNU General Public License
along with this ICMPTX. If not, see <http://www.gnu.org/licenses/>.

### Installation
Precompiled packages are available for [Debian](https://packages.debian.org/search?keywords=icmptx&exact=1) and [Ubuntu](https://launchpad.net/ubuntu/+source/icmptx):
```shell
$ sudo apt install icmptx
```

#### Manual installation
First, make sure you have the tun module from your 2.6 kernel loaded up
or compiled into your kernel on both ends of your tunnel.

Second, compile the code on both the client machine and the server you
wish to tunnel your traffic between.

### Basic usage instructions
#### Synopsis
```shell
icmptx [ -s | -c ] server
```
#### Options
```shell
       -s     Server mode
       -c     Client mode
```
#### Usage
On the server side, do something like
```shell
./icmpx -s 1.2.3.4 &
sleep 1
ifconfig tun0 10.0.3.1 netmask 255.255.255.0
```

On the client side, do something like
```shell
./icmptx -c 1.2.3.4 &
sleep 1
ifconfig tun0 10.0.3.2 netmask 255.255.255.0
```

Replace `1.2.3.4` with your internet-accessible IP on the server. At this
point you should have a simple link between the client and server. On
the client, you should be able to ping `10.0.3.1` and get a response.

Note
that there are several levels of irony involved in receiving the responses.
SSH tunneling can be used at this point for secure communication over the
channel. Note that there is no encryption capability provided directly by
ICMPTX.

Once you've confirmed that the tunnel does in fact work, routing should be
easily accomplished. The tun interfaces are just like any other ethernet
devices on your system and can be used as such, for example:
```shell
route add -net 192.168.0.0/24 gw 10.0.3.1
```

Executed on the client could add a route to your server's DMZ segment.
Access to systems on the `192.168.0.0/24` subnet from the client would
then be transparently tunneled through the ICMPTX connection.

### Demo
If you learn by watching videos, check out this screencast by [@TallPaulTech](mailto:cwne88@tutanota.com) to explore `ICMPTX` features.

<a title="Bypassing Firewalls With PING!" href="https://www.youtube.com/watch?v=VNRr5qAA_l4">
  <img src="https://i3.ytimg.com/vi/VNRr5qAA_l4/hqdefault.jpg" width="480">
</a>

### Who's to blame for all this?
ICMPTX has an interesting lineage. The code for the ICMP handling was
originally included from the itunnel program. Tun interface handling
was included from the VTun project, originally authored by Maxim
Krasnyansky. The two were brought together by edi / teso.

From there,
Siim Põder cleaned up the code and wrote a short article about it
at [(archived) http://www.linuxexposed.com/content/view/153/52/](https://web.archive.org/web/20100501002232/http://www.linuxexposed.com/content/view/153/52/).
That seems to be where Thomer Gil found it, after which he further cleaned
it up and presented it at <https://thomer.com/icmptx/>, which is where I,
JS Moore, came across it. Further and newer information may be available
available at the project's homepage at <https://codeberg.org/jakkarth/icmptx>.

### GitHub issues
This project was previously hosted at GitHub. We urge you to read about the
[Give up GitHub](https://GiveUpGitHub.org) campaign from
[the Software Freedom Conservancy](https://sfconservancy.org) to understand
some of the reasons why GitHub is not a good place to host FOSS projects.

Any use of this project's code by GitHub Copilot, past or present, is done
without our permission. We do not consent to GitHub's use of this project's
code in Copilot.

